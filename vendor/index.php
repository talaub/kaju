<?php
$requested_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$config_string = file_get_contents("../configuration/config.json");
$routes_string = file_get_contents("../configuration/routes.json");

$config = json_decode($config_string, true);
$routes = json_decode($routes_string, true);

$subpath = substr($requested_path, strlen($config["base"]));

$found = false;

$req_parts = explode("/", $subpath);

foreach ($routes as $route) {
    $route_parts = explode("/", $route["url"]);
    $found = true;
    foreach ($route_parts as $idx => $rp) {
        if (strlen($rp) === 0)
            continue;

        if ($idx > count($req_parts) - 1) {
            $found = false;
            break;
        }

        if ($rp[0] == ":" || $rp[0] == "*")
            continue;
        error_log("$rp !== {$req_parts[$idx]}");
        if ($rp !== $req_parts[$idx]) {
            $found=false;
            break;
        }
    }
    if ($found)
        break;
}
error_log($found? "true": "false");
if (!$found || $subpath === "/404") {
    error_log("IN 404");
    header("HTTP/1.0 404 Not Found");
    echo "<script>window.notFound=true;</script>";
}

$base_slashes = substr_count($config["base"], "/");
$slashes = substr_count($requested_path, "/") - ($base_slashes + 1);
$BACK = "";

for ($i=0; $i<$slashes; $i++)
    $BACK .= "../";


// Variables
$title = "Kaju";

?>
<!doctype html>
<html>
<head>
    <title><?=$title?></title>
    <script src="<?= $BACK ?>dist/script.min.js"></script>
</head>
<body>
</body>
</html>