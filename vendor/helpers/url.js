module.exports = (url) => {
    let config = require('../../configuration/config.json')
    return `${config.base}public/${url}`
}