module.exports = (string) => {
    let lang = require("../translator")
    let _ = require('../lib/lib')
    return _.fetchFromObject(lang, string)
}