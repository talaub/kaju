module.exports = (route, options) => {
    let config = require("../../configuration/config.json")
    let Handlebars = require('handlebars')
    return new Handlebars.SafeString(`<a href="${config.base}${route}">${options.fn(this)}</a>`)
}