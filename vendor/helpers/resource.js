module.exports = (route, options) => {
    let config = require("../../configuration/config.json")
    let Handlebars = require('handlebars')
    return new Handlebars.SafeString(`${config.base}/resources/${route}`)
}