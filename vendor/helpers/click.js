module.exports = (action) => {
    let Handlebars = require('handlebars')
    return new Handlebars.SafeString(`onclick="window.controller.actions.${action}()"`)
}