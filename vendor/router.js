let page = require('page')
let $ = require('jquery')
let _ = require('Kaju')
let Handlebars = require('handlebars/runtime')

let config = require('../configuration/config.json')
let redirects = require('../configuration/redirects.json')
let routes = require('../configuration/routes.json')

page.base(`${config.base}`)

let lang = require('./translator')

let generated_routes = {}

for (let route of routes) {
    generated_routes[route.url] = (ctx, next) => {

        let url_params = {}
        let url_args = {}
        if (ctx) {
            for (let pair of ctx.querystring.split("&")) {
                if (pair.indexOf("=") >= 0) {
                    let [key, value] = pair.split("=")
                    url_params[`${key}`] = value;
                }
                else
                    url_params[`${pair}`] = true
            }
            url_args = ctx.params
        }

        window.controller = require(`../src/controller/${route.controller}`)
        window.context = window.controller.init()
        window.context.set = (key, value, notify_observer = true) => {
            delete window.context[key]
            window.context[key] = value

            if (window.controller.observers[key] !== undefined && notify_observer)
                window.controller.observers[key]()
        }

        window.context.url_params = url_params
        window.context.url_args = url_args

        for (let [key, value] of Object.entries(window.controller)) {
            if (['init', 'onload', 'actions', 'observers'].indexOf(key) < 0 && Object.keys(window.context).indexOf(key) < 0)
                window.context.set(key, value, false)
        }

        // Load all components
        let components = require('./lib/components')
        for (let [name, func] of Object.entries(components)) {
            Handlebars.registerHelper(`components_${name}`, func)
        }

        // Set the title of the page
        $('html>head>title').text(_.fetchFromObject(lang, route.title))

        let template = require(`../templates/${route.template}`)

        let main_context = {content: template(window.context), page_context: window.context}

        let main_template = require('../template.hbs')


        $(document).ready(() => {
            $('html>body').html((route.use_template === undefined || route.use_template === true) ? main_template(main_context) : template(window.context))
            window.controller.onload()
        })

    }

}

for (let [url, func] of Object.entries(generated_routes))
    page(url, func)

for (let [from, to] of Object.entries(redirects))
    page.redirect(from, to)

page()

if (window.notFound) {
    generated_routes["/404"]()
}
