
let lang = require('../translator')

module.exports = {
    fetchFromObject: function (obj, prop) {
        if(typeof obj === undefined) {
            return false;
        }

        let _index = prop.indexOf('.')
        if(_index > -1) {
            return this.fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
        }

        return obj[prop];
    },
    async: (callback) => {
        window.setTimeout(callback, 0)
    },

    api: require('./restapi'),
    restapi: require('./restapi'),
    language: lang

}