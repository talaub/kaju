let Handlebars = require('handlebars');

let out = {}

// Loads all model files from the models-directory
let req = require.context("../../templates/components", true, /\.hbs$/);
let keys = req.keys
req = undefined

keys().forEach(function(key){
    let subKey = key.trim().substr(2)
    out[`${subKey.substr(0, subKey.length - 4)}`] = function () {
        // Get the hash/options object
        let hash = arguments[arguments.length - 1]

        // Get all parameters
        let params = []
        for (let i = 0; i < arguments.length - 1; i++)
            params.push(arguments[i])

        let context = {options: {}}

        context.parameters = params
        for (let [key, value] of Object.entries(hash.hash)) {
            if (['hash', 'fn', 'inverse', 'data'].indexOf(key) < 0)
                context.options[key] = value
        }
        if (hash.fn) {
            context.content = hash.fn(this)
            context.options = hash.hash
        }

        let template = require(`../../templates/components/${subKey}`)
        return new Handlebars.SafeString(template(context))

    }
})

module.exports = out
