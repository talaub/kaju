let config = require('../../configuration/config.json')
let $ = require('jquery')

function getParameterString (parameters) {
    // Builds the url parameter string
    let params = "?";
    for (let [key, value] of parameters)
        params += `${key}=${value}&`
    // Cuts the last char which is either ? or &
    return params.substr(0, params.length - 1)
}

module.export = {
    get: (url_extension, parameters, callback, headers) => {
        let params = getParameterString(parameters)

        let url = `${config.api_base}${url_extension}${params}`
        $.ajax({
            type: "GET",
            beforeSend: (request) => {
              for (let [key, value] of headers)
                  request.setRequestHeader(key, value)
            },
            url: url
        }).done(callback)
    },
    post: (url_extension, parameters, callback, headers, data) => {
        let params = getParameterString(parameters)

        let url = `${config.api_base}${url_extension}${params}`
        $.ajax({
            type: "POST",
            beforeSend: (request) => {
                for (let [key, value] of headers)
                    request.setRequestHeader(key, value)
            },
            data: data,
            url: url
        }).done(callback)
    },
    delete: (url_extension, parameters, callback, headers) => {
        let params = getParameterString(parameters)

        let url = `${config.api_base}${url_extension}${params}`
        $.ajax({
            type: "DELETE",
            beforeSend: (request) => {
                for (let [key, value] of headers)
                    request.setRequestHeader(key, value)
            },
            url: url
        }).done(callback)
    },
    put: (url_extension, parameters, callback, headers) => {
        let params = getParameterString(parameters)

        let url = `${config.api_base}${url_extension}${params}`
        $.ajax({
            type: "PUT",
            beforeSend: (request) => {
                for (let [key, value] of headers)
                    request.setRequestHeader(key, value)
            },
            url: url
        }).done(callback)
    },
    patch: (url_extension, parameters, callback, headers) => {
        let params = getParameterString(parameters)

        let url = `${config.api_base}${url_extension}${params}`
        $.ajax({
            type: "PATCH",
            beforeSend: (request) => {
                for (let [key, value] of headers)
                    request.setRequestHeader(key, value)
            },
            url: url
        }).done(callback)
    }
}