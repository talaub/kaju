let out = {}

// Loads all model files from the models-directory
let req = require.context("../../src/services", true, /\.js$/);
let keys = req.keys
req = undefined
keys().forEach(function(key){
    let subKey = key.trim().substr(2, key.trim().length - 5)
    out[`${subKey}`] = require(`../../src/services/${subKey}`)
});

module.exports = out
