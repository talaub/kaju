:product: Kaju
:Author: Tamino Laub
:Email: t.laub@sir-web.com
:Date: 24.02.2018
:Revision: 0.0.1
:Company: sir-web.com
:Template Engine: Handlebars
:Router: page.js
:ext-relative: {outfilesuffix}

= Setup

To setup {product} you need yarn and webpack installed on your machine. Run `yarn install` to fetch all dependencies.
After that you can start setting up your project. Keep webpack running in the background using `webpack --watch`.

== Necessary files

{product} needs some files to work propery. Let's start with the configuration directory.
Make sure that it contains these two files: routes.json and config.json.

The routes.json should contain an empty JSON array by default.
And the config.json an JSON object with some basic values.

=== Config

The config.json has some really important values. The base property tells {product} in which directory it is executed. This info is important for routing. Set it to `""` if you run Kaju in the root of your webserver. If you run it in the /kaju directory the value should be `/kaju`.

Another important property is the api_base property. If you are building a frontend for you restapi you can specify the url of the restapi here.

NOTE: Use the full URL, don't use a relative path and don't set it to localhost.

The css property tells {product} your root css-file. This property is relative to the stylesheet directory.