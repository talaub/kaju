const path = require('path')

module.exports = {
    entry: "./src/app.js",

    output: {
        path: path.resolve(__dirname, "dist"),

        filename: "script.min.js"
    },
    module: {
        rules: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/, query: { presets: ['babel-preset-es2015'].map(require.resolve) } },
            {test: /\.hbs$/, loader: "handlebars-loader", options: { helperDirs: [__dirname + "/vendor/helpers", __dirname + "/src/helpers"], precompileOptions: {knownHelpersOnly: false} }},
            {test: /\.ya?ml$/, use: "js-yaml-loader"},
            {test: /\.css$/, use: ["style-loader","css-loader"]}
        ]
    },
    node: {
        fs: 'empty'
    },
    stats: {
        warnings: false
    },
    resolve: {

        extensions: ['.tsx', '.ts', '.js', '.json', '.css'],

        alias: {
            Kaju: path.resolve(__dirname, 'vendor/lib/lib.js'),
            Models: path.resolve(__dirname, 'vendor/lib/models.js'),
            Services: path.resolve(__dirname, 'vendor/lib/services.js')
        }
    }
}